--- 
author: Céleste Clavier--Dupérat
title: Oral de stage
subtitle: https://s.42l.fr/3B
---

# {data-background-image="./includes/moi.jpg" data-state="white90"}

<table>
    <tr>
        <td style="vertical-align: middle;">Céleste Clavier--Dupérat</td>     
        <td><img src="./includes/moi.jpg" style="width: 160px"> </td>   
    </tr> 
</table>


<small>

| | |
|-|-|
|<i class="fa fa-envelope"></i> | celeste@tcweb.org |
|<i class="fa fa-instagram"></i> | celeste.c_d |
|<i class="fa fa-phone"></i>    | +33 7 69 67 16 66 |

</small>

# Crise COVID19 {data-background-image="./includes/covid19.jpg" data-state="white70"}

* Septembre : je recherche un stage
* 7 Octobre : j'ai mon stage
* 1 Décembre : mon stage est annulé
* Décembre : je recherche un stage
* 6 Janvier : Merci Mme Mollet

# Le cabinet de Maître Breyer {data-background-image="./includes/plaque2.JPG" data-state="white70"}

* Avocate
* À Roubaix
* En droit des affaires

# Le métier {data-background-image="./includes/avocat.jpg" data-state="white80"}

:::: {.columns}
::: {.column width="50%"}
* Problème 
* Préparation du dossier
* Procé
:::
::: {.column width="50%"}
* Demande
* Étude
* Solution
:::
::::

# Les qualités requises {data-background-image="./includes/symboles.jpg" data-state="white70"}

# Formations et diplômes {data-background-image="./includes/lille3.jpg" data-state="white80"}

* Master de droit : BAC +5
* Certificat d'aptitude à la profession d'avocat (CAPA) : 18 mois

# Mes activités {data-background-image="./includes/dossier.jpg" data-state="white70"}

# Complément de stage {data-background-image="./includes/bureau.jpg" data-state="white70"}

# Conclusion {data-background-image="./includes/conclu.jpg" data-state="white70"}
